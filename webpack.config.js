const MiniCssExtractPlugin = require("mini-css-extract-plugin"),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	ScriptExtHtmlWebpackPlugin = require("script-ext-html-webpack-plugin");

module.exports = {
	mode: "development",
	devtool: "inline-source-map",
	output: {
		filename: "index.js"
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js"]
	},
	module: {
		rules: [
			{ test: /\.tsx?$/, loader: "ts-loader" },
			{ test: /\.scss$/, use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"] }
		]
	},
	plugins: [
		new MiniCssExtractPlugin(),
		new HtmlWebpackPlugin({
			title: "Clue Helper", inject: "head",
			meta: { viewport: 'width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no' },
			xhtml: true
		}), new ScriptExtHtmlWebpackPlugin({
			defaultAttribute: 'async'
		})
	],
	devServer: {
		host: '0.0.0.0',
		disableHostCheck: true
	}
};
